package com.webVillage.app.webControllers;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.List;
import java.util.Random;
import java.util.function.BooleanSupplier;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.contains;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class GreetingControllerTest {

    private WebDriver driver;
    private String startPage = "http://localhost:";

    @LocalServerPort
    private int port;

    @BeforeAll
    public static void setProperty() {
        System.setProperty("webdriver.chrome.driver",
                "C:\\Users\\79274\\Documents\\chromedriver.exe");
    }

    @BeforeEach
    public void beforeTest() {
        driver = new ChromeDriver();
        driver.get(startPage + port + "/greeting");
    }

    @AfterEach
    public void afterTest() {
        driver.quit();
    }

    @ParameterizedTest
    @CsvSource(value = {
            "//h3;;Hello, World!",
            "//h3[2];;Find me!",
            "//h3[@class='header'];;And me!",
            "//h3[@class='header'][2];;NOW me!",
            "//h3[@class='header yellow'];;Do not forget about me",
            "//h3[@class='header'][@thestyle='red'];;It is simple",
            "//h3[@class='header'][contains(@thestyle,'e')];;It is simple",
            "//h3[@class='header'][starts-with(@thestyle,'r')];;It is simple",
            "//h3[@class='header'][substring(@thestyle, string-length(@thestyle) - string-length('d') + 1) " +
                                                                                      " = 'd'];;It is simple",
            "//h3[contains(@class, 'abracadabra')][contains(@class, 'kitty-cat')];;My turn",
            "//h3[contains(@class, 'abracadabra')][contains(@class, 'bow-wow-dog')];;Where am I...",
            "//h3[not(contains(@class, 'abracadabra'))][contains(@class, 'kitty-cat')];;What about me",
            "//div[@id='good']/h3;;I am hiding",
            "//div[@id='bad']/h3;;U cannot get me!",
            "//div[@id='ugly']//h3;;Where is Blondie again?",
            "//p[@id='not_me'];;Find my daddy please.",
            "//p[@id='iAmLost']/ancestor::h3;;Look for my granny!",
            "//h3[@id='sister'];;Sister",
            "//h3[@id='sister']/../..//h3[not(@id='sister')];;Brother",
            "//div[@id='last']//h3[last()];;And me, please",
            "//div[@id='last']//h3[last()-1];;Find me!",
            "//h3[@price!=5];;Find me because my price is not 5",
            "//h3[@age>4];;Find me because my age is bigger than 4",
            "//h3[@courage>5 and @courage<15];;Find me because my courage is between 5 and 15",
            "//h3[text()='U can find me by text'];;U can find me by text",
            "//h3[contains(text(),'part of the text')];;U can find me by the part of the text"
    },
            delimiterString = ";;")
    void greeting(String xpath, String expectedResult) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath(xpath));
        String text = element.getText();
        System.out.println(driver.getPageSource());
        Assertions.assertEquals(expectedResult, text);

    }

    @ParameterizedTest
    @CsvSource(value = {
            "h3;;Hello, World!",
            "h3:nth-child(2);;Find me!",
            "h3.header;;And me!",
            "h3.header:nth-child(4);;NOW me!",
            "h3.header.yellow;;Do not forget about me",
            "h3.header[thestyle*='e'];;It is simple",
            "h3.header[thestyle$='d'];;It is simple",
            "h3.header[thestyle^='r'];;It is simple",
            "h3.kitty-cat.abracadabra;;My turn",
            "h3.bow-wow-dog;;Where am I...",
            "h3.kitty-cat:not(.abracadabra);;What about me",
            "div#good h3;;I am hiding",
            "div#bad h3;;U cannot get me!",
            "div#ugly h3;;Where is Blondie again?",
            "h3#sister;;Sister",
            "div#last h3:last-child;;And me, please",
            "div#last h3:nth-last-child(2);;Find me!",
            "h3[price]:not([price='5']);;Find me because my price is not 5",
    },
            delimiterString = ";;")
    void greetingCss(String css, String expectedResult) throws InterruptedException {
        WebElement element = driver.findElement(By.cssSelector(css));
        String text = element.getText();
        System.out.println(driver.getPageSource());
        Assertions.assertEquals(expectedResult, text);
    }
}